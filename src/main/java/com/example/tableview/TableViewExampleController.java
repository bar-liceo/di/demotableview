package com.example.tableview;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TableViewExampleController {

    @FXML
    private TableView tablePerson;

    private ObservableList<Person> personsObs;

    private static final List<String> NAMES = new ArrayList<String> (Arrays.asList(
            "Tigernach",
            "Margetud",
            "Sadb",
            "Senán",
            "Muirgel",
            "Rathnat",
            "Seisyll",
            "Damnat",
            "Arthmail",
            "Cian",
            "Catguocaun",
            "Enniaun",
            "Luigsech",
            "Finnán",
            "Fintan "));

    private static final List<String> LAST_NAMES = new ArrayList<String> (Arrays.asList(
            "Ardgal",
            "Murchad",
            "Declán",
            "Bébinn",
            "Oébfinn",
            "Cóemgein",
            "Morcant",
            "Comgall",
            "Ségdae",
            "Dálach",
            "Tigernán",
            "Áednat",
            "Óengus",
            "Margetud",
            "Cathal",
            "Maeleachlainn",
            "Cathal",
            "Dumnorix",
            "Flannán"));

    private String getRandomItem(List<String> list){
        Random rand = new Random();
        int n = rand.nextInt(list.size());
        return list.get(n);
    }
    private Person genRandomPerson(){
        return new Person(getRandomItem(NAMES), getRandomItem(LAST_NAMES));
    }
    @FXML
    protected void onRefreshButtonClick() {
        Random rand = new Random();
        int sizeTable = rand.nextInt(50);
        List<Person> persons = new ArrayList<Person>();
        for (int i = 0; i < sizeTable; i++) {
            persons.add(genRandomPerson());
        }
        updatePersonObj(persons);
        System.out.println("Refreshing list");
    }

    private void updatePersonObj(List<Person> persons){
        this.personsObs = FXCollections.observableList(persons);
        this.tablePerson.setItems(this.personsObs);
    }

    public void initialize(){
        TableColumn<Person, String> column1 =
                new TableColumn<>("First Name");

        column1.setCellValueFactory(
                new PropertyValueFactory<>("firstName"));
        
        TableColumn<Person, String> column2 =
                new TableColumn<>("Last Name");

        column2.setCellValueFactory(
                new PropertyValueFactory<>("lastName"));

        TableColumn<Person, String> column3 = new TableColumn<>("Actions");

        List<Person> persons = new ArrayList<Person>();

        for (int i = 0; i < 10; i++) {
            persons.add(new Person("Person " + i +" name", "Person " + i + " last name"));
        }

        tablePerson.getColumns().add(column1);
        tablePerson.getColumns().add(column2);


        updatePersonObj(persons);

    }
}